function  getDiffrenceHour ($diffrence,$startTime) {
    $startTime = explode(' ', $startTime);
    date_default_timezone_set("Europe/Istanbul");
    $endTime = date('H:i:s');
    $result = strtotime($endTime) - strtotime($startTime[1]);
    if ($result >=  $diffrence) {
        return true;
    }
    else {
        return false;
    }
}

#Example 
echo getDiffrenceHour(3600,'2018-06-01 19:28:11'); #3600 second = 1 hour
